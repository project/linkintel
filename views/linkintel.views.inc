<?php

/**
 * @file Similar By Terms Views data include file
 */

function linkintel_views_data() {
  
  $data['linkintel_link']['table']['group']  = t('Link Intelligence');
  $data['linkintel_link']['table']['base'] = array(
    'field' => 'lid',
    'title' => t('Link Link'),
    'help' => t('Link links on your site.'),
  );
  $data['linkintel_link']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'linkintel_keyword' => array(
      'left_field' => 'kid',
      'field' => 'kid',
    ),
  );
  $data['linkintel_link']['from_path'] = array(
    'title' => t('From path'), // The item it appears as on the UI,
    'help' => t('The path of the page the link is from.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['linkintel_link']['from_nid'] = array(
    'title' => t('From nid'),
    'help' => t('The node ID of the page the link is from.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['linkintel_keyword']['keyword'] = array(
    'title' => t('Keyword'),
    'help' => t('The keyword of the link.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['linkintel_link']['text'] = array(
    'title' => t('Text'), // The item it appears as on the UI,
    'help' => t('Text of the link.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['linkintel_link']['path'] = array(
    'title' => t('Link URL'), // The item it appears as on the UI,
    'help' => t('The href of the link.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['linkintel_link']['the_lid'] = array(
    'real field' => 'lid',
    'title' => t('Link'), // The item it appears as on the UI,
    'help' => t('Complete link.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'linkintel_handler_field_link',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['linkintel_link']['mode'] = array(
    'title' => t('Mode'), // The item it appears as on the UI,
    'help' => t('Text of the link.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'group' => t('Link Intelligence'), // The group it appears in on the UI. Could be left out.
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function linkintel_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'linkintel') . '/views',
    ),
    'handlers' => array(
      'linkintel_handler_field_link' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}