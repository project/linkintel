; $Id $

DESCRIPTION
--------------------------
The Link Intelligence (LI) module helps you manage links and enables more intelli-gent linkintelng of related content. 

INSTALLATION
---------------
- Upload the LI files a modules directory on your server such as sites/all/modules/linkintel.

- Enable the Link Intelligence module via the Admin > Site building > Modules
  - If you want to display sidebar (block) links in your content area, enable the Link Intelligence CCK Field module

- Configure the module by going to Admin -> Settings -> Link Intelligence
    - If you don’t already have a vocabulary for keywords, it is recommended that you create on to use with LI’s request sync and suggestion seeding features.

- Enable and configure LI defaults per content type by going to Admin -> Content -> Content Types and editing each content type. Note that you can set configuration per node by using the Link Intelligence field set on the node edit form.

- You can access LI tools in two places. Site wide data can be found at Admin -> Content -> Link Intelligence and node specific tools on the Link Intel tab on nodes.

DEPENDENCIES & RECOMMENDATIONS
---------------
LI has no module dependencies. 

It is recommended to use LI in conjunction with the Keyword Research module for en-hanced control over link priority scoring.
http://drupal.org/project/kwresearch 

It is also recommended to use LI with Alchemy to provide enhanced suggestion key-word seeds.
http://drupal.org/project/alchemy 

CREDITS
----------------------------
Authored and maintained by Tom McCracken <tom AT leveltendesign DOT com> twitter: @levelten_tom
Sponsored by LevelTen Interactive - http://www.leveltendesign.com